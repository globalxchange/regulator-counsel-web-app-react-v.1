import React, { useState, useContext } from 'react';
import Sider from 'antd/lib/layout/Sider';
import { ReactComponent as IconPlus } from '../../static/images/sidebar-icons/plus.svg';
import { ReactComponent as IconOver } from '../../static/images/sidebar-icons/dash.svg';
import msgIcon from '../../static/images/sidebar-icons/msg-icon.svg';
import logo from '../../static/images/sidebar-icons/logo/logo.svg';
import logoFull from '../../static/images/sidebar-icons/logo/logo-full.svg';
import { CounselAppContext } from '../../Context_Api/Context';
import { Link } from 'react-router-dom';

function Sidebar({ active }) {
  const { login, profilePic, profileName } = useContext(CounselAppContext);
  const [collapsed, setCollapsed] = useState(false);
  const onCollapse = (collapsed) => {
    console.log(collapsed);
    setCollapsed(collapsed);
  };
  return (
    <Sider
      className="bets-dash-sider"
      theme="light"
      collapsible
      collapsed={collapsed}
      onCollapse={onCollapse}
      width={280}
      trigger={null}
    >
      <div
        className={'logo ' + collapsed}
        onClick={() => setCollapsed(!collapsed)}
      >
        {collapsed ? <img src={logo} alt="" /> : <img src={logoFull} alt="" />}
      </div>
      <div className={'sider-contents ' + collapsed}>
        <div className="group">
          <h4>Marketplace</h4>
          <Link
            to="/marketplace1"
            className={
              'menu-itm' + (active === 'marketplace1' ? ' active' : '')
            }
          >
            <IconOver /> <h5>Marketplace 1</h5>
          </Link>
          <div className="menu-itm mt-auto">
            <IconPlus /> <h5>Create</h5>
          </div>
        </div>
        <div className="group">
          <h4>Management</h4>
          <Link
            to="/management1"
            className={'menu-itm' + (active === 'management1' ? ' active' : '')}
          >
            <IconOver /> <h5>Management 1</h5>
          </Link>
        </div>
        <div className="group" style={{ flexGrow: 1 }}>
          <h4>Apps</h4>
          <Link
            to="/myapps1"
            className={'menu-itm' + (active === 'myapps1' ? ' active' : '')}
          >
            <IconOver />
            <h5>My App 1</h5>
          </Link>
        </div>
        <div className="profile-logout">
          <img className="dp" src={profilePic} alt="" />
          <div className="name px-2">
            <div className="">
              <h4>{profileName}</h4>
              <Link
                to="/login"
                onClick={() => {
                  login();
                }}
              >
                Log out
              </Link>
            </div>
            <div className="msg">
              <div className="count">0</div>
              <img src={msgIcon} alt="" />
            </div>
          </div>
        </div>
      </div>
    </Sider>
  );
}

export default Sidebar;
